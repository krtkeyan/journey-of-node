
EXPRESS:

Rewrite the HTTP server application using Express.js


Reference:
 1) https://medium.freecodecamp.org/getting-off-the-ground-with-expressjs-89ada7ef4e59

BONUS: 

 - Learn to install express js using npm modules. 
 - Learn what is dependencies and save dependencies.
 - Learn about package.json
 - Learn about express middlewares.
