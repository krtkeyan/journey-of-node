var express = require('express');
var app     = express();
var path    = require('path');

app.get('/',function(request,response){

	response.sendFile(path.join(__dirname ,'assets/index.html'));

});

app.listen(3030);

console.log('Server Running on 3030');
