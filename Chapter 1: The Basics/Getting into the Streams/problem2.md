
READ FILE USING STREAMS AND PIPE:

  Use createReadStream() function and file contents(input.txt) using streams.

  Pipe the output into the terminal screen.

  HINTS:

   Create a readStream variable and assign it to createReadStream(filename)

   Pipe readStream into process.stdout to output into terminal.


  REFERENCE DOCS: https://nodejs.org/dist/latest-v6.x/docs/api/fs.html#fs_fs_createreadstream_path_options