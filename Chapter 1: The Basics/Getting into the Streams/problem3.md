
READ FILE USING STREAMS:

 This program is same as the problem1 instead of using readFile() function use createReadStream().Use event emitters to process the file content.

 
 HINT:

 Create a readStream variable and assign it to createReadStream(filename)

 readStream
  .on('data', function (chunk) {
    //COUNT THE CHARACTERS
  })
  .on('end', function () {
    //OUTPUT INTO TERMINAL CONSOLE 
  })