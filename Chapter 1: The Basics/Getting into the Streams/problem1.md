USING FS MODULES:

READ FILE ASYNCHROUSLY:

  Write a Program to read the file (input.txt) and display its contents and number of characters.

HINTS:
 
  Use fs module and readFile() function to read file asynchronously and use callbacks to return the results and print the output.

  REFERENCE DOCS: https://nodejs.org/dist/latest-v6.x/docs/api/fs.html#fs_fs_readfile_file_options_callback