IMPLEMENT A HYPER TEXT TRANSFER PROTOCOL (HTTP) SERVER:

    - HTTP server is the same as the FTP server. Except it's going to serve the html files.

    In this program you should use http module to serve html files in the assets folder. And view the index page in your browser.

   
   Write the http server using http module and createServer() function.

   In your callback read the index.html file in the asset and send to the client.
    

Reference: 
1) https://blog.risingstack.com/your-first-node-js-http-server/
2) https://nodejs.org/en/docs/guides/anatomy-of-an-http-transaction/
3) https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview

