var http = require('http');
var fs   = require('fs');

var server = http.createServer(function (request,response){

	fs.readFile("assets/index.html" , function(err, html_content){

		if(err){
		   response.writeHead(404);
		   response.write('Content Not Found');
		   
		}else{
		    response.writeHead(200,{'Content-Type':'text/html'});
		    response.write(html_content.toString());
		    console.log(html_content);
		      
		}
	
		response.end();
       });
});

server.listen(3030);

console.log('Server started Listen in 3030');
