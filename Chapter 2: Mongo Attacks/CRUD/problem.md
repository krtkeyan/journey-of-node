
INITIALISATION:

    Initialise the MongoDb and console successful message on connection

   - Create a MongoDB Client and connect to the database.
   - Write A CRUD application with Express JS after connecting to the database.
   - Learn to use POSTMAN client to request and your express app should return the response.


Install mongo:

 - Install and Run MongoDB with Homebrew
 - Open the Terminal app and brew install mongodb.
 - To connect to Remote mongoDB:

   mongo ds129811.mlab.com:29811/mongoattacks -u rams -p mongotest00

   RemoteMongoURL: ds129811.mlab.com:29811/mongoattacks
   username: rams
   password: mongotest00

   Use the same in your mongoclient node app.

 - type show collections to list databases/collections;
 - type db.Inventory.find() to list documents in Inventory database/collection.
 
 - To stop the Mongo daemon hit ctrl-c
 
Reference:

Documentation: https://docs.mongodb.com/manual/crud/
Part 1 - https://zellwk.com/blog/crud-express-mongodb/
Part 2 - https://medium.freecodecamp.org/building-a-crud-application-with-express-and-mongodb-part-2-11d421bb0215
